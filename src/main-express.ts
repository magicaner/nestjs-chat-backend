import {Logger} from '@nestjs/common';
import {NestFactory} from '@nestjs/core';
import {ConfigService} from '@nestjs/config';
import express from "express";

import {AppModule} from './app.module';
import {HttpExceptionFilter} from "./filters/http-exception.filter";
import {application} from "./utils/application.utils";

export default async function bootstrap() {
    const app = await NestFactory.create(AppModule,{
        snapshot: true,
        logger: ["verbose"]
    });

    app.enableCors({
        "origin": "*",
        "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
        "preflightContinue": false,
        "optionsSuccessStatus": 204
    });

    application.init(app);

    app.useGlobalFilters(new HttpExceptionFilter());
    const configService: ConfigService = app.get<ConfigService>(ConfigService);
    const port = configService.get('PORT');
    app.setGlobalPrefix(configService.get('BASE_PREFIX'));

    app.use(express.json({limit: '50mb'}));
    app.use(express.urlencoded({limit: '50mb'}));
    await app.listen(port, '0.0.0.0');
    Logger.log(`Application is running on: ${await app.getUrl()}`, 'Application');
}
