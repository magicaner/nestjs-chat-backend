import _ from "lodash";

export interface ConfigProviderInterface {
    save(): Promise<this>;
    refresh(): Promise<this>;
    getData(): {[key:string]: any};
    setData(data: {[key:string]: any}): this;
    addData(data: {[key:string]: any}): this
    has(path: string, checkIfEmpty: boolean): boolean
    get(path: string, defaultValue: any): any;
    set(path: string, value: any): this;
}
