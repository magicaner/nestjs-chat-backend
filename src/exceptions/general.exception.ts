import {AbstractException} from "./abstract.exception";

export class GeneralException extends AbstractException
{
    constructor(message: string|any = 'Internal error', code: string = 'GENERAL_ERROR')
    {
        super(message, code);
    }
}
