export class AbstractException extends Error
{
    public code: string;
    public message: string|any;
    constructor(message?: string|any, code?: string)
    {
        super();
        if (message) {
            this.message = message;
        }
        if (code) {
            this.code = code;
        }

    }

    public setMessage(message: string): this
    {
        this.message = message;
        return this;
    }

    public setCode(code: string): this
    {
        this.code = code;
        return this;
    }

    public getCode(): string
    {
        return this.code;
    }

    public getMessage(): string
    {
        return this.message;
    }

}
