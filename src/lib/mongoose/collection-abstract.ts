import {Logger} from '@nestjs/common';
import {HydratedDocument, Model} from "mongoose";
import mongoose from 'mongoose'
import {isArray, isEmpty, isObject, isString} from "lodash";
import {GeneralException} from "../../exceptions/general.exception";
import {Sort} from "./request-types";

export const DEFAULT_SORT_DIRECTION = 'asc';

export type SortType = {
    field: string,
    direction: string
}

export abstract class CollectionAbstract<ModelType, DocumentType>
{
    protected items = [];
    protected isLoaded = false;
    protected filter: any = {};
    protected totalRecords = 0;
    protected pageSize = 20;
    protected pageNum = 1;
    protected orderBy: SortType[] = [];
    protected aggregationResult: any;
    protected model: Model<ModelType>;

    public constructor(
        model?: Model<ModelType>
    ) {
        this.filter = {};
        this.model = model;
    }

    public createNewItem(): DocumentType
    {
        let document = new this.model() as DocumentType;
        return document;
    }

    public abstract getDefaultOrder(): SortType | null


    public setPageSize(pageSize: number): this
    {
        this.pageSize = pageSize;
        return this;
    }

    public addOrder(field: any|SortType, direction?: string, mode?: string): this
    {
        direction = direction ?? DEFAULT_SORT_DIRECTION;
        switch (true) {
            case isArray(field):
                for (let row of field) {
                    this.orderBy.push({
                        'field': row.field,
                        'direction': row.direction ?? direction
                    });
                }
                break;
            case isString(field):
                this.orderBy.push({
                    'field': field,
                    'direction': direction
                });
                break;
            case isObject(field):
                this.orderBy.push({
                    'field': field['field'],
                    'direction': field['direction'] ?? direction
                });
                break;
        }
        return this;
    }

    public setPageNum(pageNum: number): this {
        this.pageNum = pageNum;
        return this;
    }

    public getPageSize(): number {
        return this.pageSize;
    }

    public getPageNum(): number {
        return this.pageNum;
    }

    public Filter(filter: Object): this {
        this.filter = filter;
        return this;
    }

    public push(model: DocumentType) {
        this.items.push(model)
    }

    public reset(): this {
        this.isLoaded = false;
        this.items = [];
        return this;
    }

    public async delete(): Promise<any> {

        let result = await this.model.deleteMany(this.filter);

        return '';
    }

    public async load(scrollId = null): Promise<this> {
        if (this.isLoaded) {
            return this;
        }

        if (isEmpty(this.orderBy)) {
            this.addOrder(this.getDefaultOrder())
        }

        try {
            let connection = mongoose.connection;
            let documents = await this.model.find(this.filter).exec();

            if (documents) {
                this.items = [];

                for (let document of documents) {
                    this.items.push(document);
                }

                this.totalRecords = 0;
                this.isLoaded = true;
            }
        } catch (e) {
            Logger.error([
                'CollectionAbstract.load',
                JSON.stringify(e)
            ]);
            throw new GeneralException(e.message);
        }

        return this;
    }

    public async getItems(): Promise<Array<DocumentType>> {
        await this.load();
        return this.items;
    }

    public async getItemByFieldName(id: any, fieldName: string): Promise<DocumentType> {
        await this.load();
        for (let item of this.items) {
            if (item.get(fieldName, null, { getters: false }) === id) {
                return item;
            }
        }

        return null;
    }

    public async getFirstItem(): Promise<DocumentType> {
        await this.load();
        if (this.items.length > 0) {
            return this.items[0];
        }
        return null;
    }

    public async getTotalRecords(): Promise<number> {
        await this.load();
        return this.totalRecords;
    }

    public async getAggregationResult(): Promise<any> {
        await this.load();
        return this.aggregationResult;
    }

    public async toJson() {
        let result = {
            totalRecords: await this.getTotalRecords(),
            items: []
        }

        let aggregationResult = await this.getAggregationResult();
        if (!isEmpty(aggregationResult)) {
            result['aggregations'] = aggregationResult;
        }

        let documents = await this.getItems();
        for (let document of documents) {
            result.items.push(document);
        }

        return result;
    }
}
