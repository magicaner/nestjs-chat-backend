import _ from "lodash";
import { Aggregate, Document, Model, Query } from "mongoose";
import {Logger} from '@nestjs/common';
import { Filter, FilterMap, FilterMapValue, Pagination, Sort, SortOrder } from "./request-types";

const TEXT_SORT = { score: { $meta: "textScore" }};
type Stage = (data: any) => any | Promise<any>;

export class RequestBase<T> {
    protected query: Query<Document[], Document, {}, T>;
    protected request: Promise<any>;
    protected aggregation: Aggregate<any>;
    protected stages: Stage[] = [];
    protected postStages: Stage[] = [];
    protected filterMap: FilterMap = {};

    constructor(
        public model: Model<T>,
        public filter: Filter = {},
        public sort: Sort[] = [],
        public pagination: Pagination = {
            size: 10,
            num: 0
        }
    ) {
        this.query = this.model.find();
        this.aggregation = model.aggregate();
    }

    public setOrder(sort: Sort[]): this {
        this.sort = sort;
        return this;
    }

    public setPagination(pagination: Pagination): this {
        this.pagination = pagination;
        return this;
    }

    public setFilter(filter: Filter): this {
        this.filter = filter;
        return this;
    }

    public setFilterMap(filterMap: FilterMap) {
        this.filterMap = filterMap;
        return this;
    }

    protected pushStage(stage: Stage): this {
        this.stages.push(stage);
        return this;
    }

    protected pushPostStage(stage: Stage): this {
        this.postStages.push(stage);
        return this;
    }

    protected setQuery(query: Query<Document[], Document, {}, T>) {
        this.query.find(query.getFilter());
        return this;
    }

    protected getQuery(): Query<Document[], Document, {}, T> {
        return this.query;
    }

    protected getSearchQueryToken(searchQuery) {
        // When doing a text search if passed a single phrase, e.g. "Hello Dan", the terms will be tokenized and
        // an OR search will be executed. Thus documents having Hello OR Dan will be returned. This produces too many
        // matches and is not desirable.
        // If the search phrase is quoted, e.g. "\"Hello Dan\"", the exact phrase will be searched. This results in
        // exact matches only. With this stemming is also not possible. This is also not desirable.
        // When quoting each token, e.g. "\"Hello\" \"Dan\"", this is treated as an AND search and stemming is applied to
        // each token. This matches documents having Hello AND Dan and includes stemming, Wheel will match Wheeler for example.
        // Ths gives the best results.
        //
        // Tokenizing the string on word boundaries, remove any quotes then filter out empty tokens
        let searchTokens = `"${
            searchQuery.split(" ")
                .map(token => token.replace(/\"/g, ""))
                .filter(token => token) // remove falsy tokens, e.g. ""
                .join("\" \"")
        }"`;

        return searchTokens;
    }

    protected prependSearchQuery(query:string = null):any {
        // Push higher ranked matches to the top of the results using the textScore ranking (set the projection here)
        // TEXT_SORT must also be passed to the sort() function to use the projection
        if (query) {
            return this.model.find({$text: {$search: this.getSearchQueryToken(query ?? '')}}, TEXT_SORT);
        }
        return this.model.find();
    }

    protected getFieldMapValue(fieldMap: FilterMap, fieldName): FilterMapValue {
        if (_.isUndefined(fieldMap[fieldName])) {
            return { name: fieldName };
        } else if (_.isString(fieldMap[fieldName])) {
            return { name: <string>fieldMap[fieldName] };
        } else if (fieldMap[fieldName] instanceof Function) {
            return {
                "name": fieldName,
                "function": <any>fieldMap[fieldName]
            };
        } else {
            return _.assign({ name: fieldName }, fieldMap[fieldName]) as FilterMapValue;
        }
    }

    protected buildQuery(filter: { [key: string]: any }): Query<any, any> {
        let queries: any = [];
        let fieldsMap = _.assign({}, this.filterMap, {
            "id": "_id"
        });
        let searchQuery;
        for (let fieldName in filter) {
            let value: any = filter[fieldName];
            let isValueEmpty = Array.isArray(value)
                ? _.isEmpty(value)
                : (_.isNull(value) || _.isUndefined(value) || value == '');

            if (isValueEmpty) {
                continue;
            }

            let fieldNameMap = this.getFieldMapValue(fieldsMap, fieldName);

            if (fieldNameMap?.ignore) {
                continue;
            }

            switch (fieldNameMap.name) {
                case "query":
                    searchQuery = value;
                    break;
                default:
                    if (fieldNameMap instanceof Function) {
                        value = fieldNameMap(fieldNameMap.name, value);
                        queries.push(value);
                    } else if (fieldNameMap?.function instanceof Function) {
                        value = fieldNameMap?.function(fieldNameMap.name, value);
                        queries.push(value);
                    } else if (fieldNameMap?.checkIfExists) {
                        queries.push({
                            $or: [
                                {
                                    [fieldNameMap.name]: { $exists: false }
                                },
                                {
                                    [fieldNameMap.name]: _.isArray(value) ? { $in: value } : value
                                }
                            ]
                        });
                    } else {
                        queries.push({ [fieldNameMap.name]: _.isArray(value) ? { $in: value } : value });
                    }
                    break;
            }
        }

        if (!_.isEmpty(queries)) {
            this.query.where().and(queries);
        }

        if (searchQuery) {
            this.query = this.prependSearchQuery(searchQuery).find(this.query.getFilter());
        }

        return this.query;
    }

    protected buildDocumentsPipeline(): Array<any> {
        let pipelinesCommon: any[] = [];
        let documentsPipelines = _.concat(pipelinesCommon, []);

        if (!_.isEmpty(this.sort)) {
            for (let sort of this.sort) {
                let sortField = sort.field;
                let sortOrder = _.includes([SortOrder.asce, SortOrder.asc], sort.direction) ? 1 : -1;
                documentsPipelines.push({ $sort: { [sortField]: sortOrder } });
            }
        }

        if (!_.isEmpty(this.pagination)) {
            documentsPipelines.push({ $skip: this.pagination.num * this.pagination.size });
            documentsPipelines.push({ $limit: this.pagination.size });
        }

        //documentsPipelines.push({ $set: { id: "$_id" } });
        //documentsPipelines.push({ $unset: ["_id", "__v"] });

        return documentsPipelines;
    }

    protected buildTotalsPipelines(): Array<any> {
        let pipelinesCommon: any[] = [];
        let totalsPipelines = _.concat(pipelinesCommon, []);

        totalsPipelines = _.concat(totalsPipelines, [{ $count: "count" }]);

        return totalsPipelines;
    }

    public build(): this {
        this
            // assign filter do the building pipeline
            .pushStage((data) => {
                return _.assign({}, this.filter);
            })

            // build initial query filter
            .pushStage((data) => {
                let query = this.buildQuery(data);
                this.aggregation.option({ allowDiskUse: true });
                this.aggregation.match(query.getFilter());
                return data;
            })

            // build documents pipeline with pagination
            .pushStage((data) => {
                return this.buildDocumentsPipeline();
            })
        ;
        return this;
    }

    public paginate(includeTotals = true): this {
        this
            .pushStage(async (documentsPipelines) => {
                let totalsPipelines = this.buildTotalsPipelines();

                let facetStage: any = {
                    documents: documentsPipelines
                };

                if (includeTotals) {
                    facetStage["totalCount"] = totalsPipelines;
                }

                let projectStage = {
                    documents: 1,
                    pageNumber: 1,
                    pageSize: 1
                };

                if (includeTotals) {
                    facetStage["totalCount"] = totalsPipelines;

                    _.assign(projectStage, {
                        totalDocuments: 1,
                        totalPages: {
                            $ceil: {
                                $divide: [
                                    "$totalDocuments",
                                    this.pagination.size
                                ]
                            }
                        }
                    });
                }

                let result: any = [
                    { $facet: facetStage },
                    {
                        $set: {
                            pageNumber: (this.pagination.num + 1),
                            pageSize: this.pagination.size,
                            totalDocuments: {
                                $cond: [
                                    { $toBool: { $arrayElemAt: ["$totalCount.count", 0] }},
                                    { $arrayElemAt: ["$totalCount.count", 0] },
                                    0
                                ]
                            },
                        }
                    },
                    { $project: projectStage}
                ];

                return result;
            })
        ;

        this
            .pushPostStage((data) => {
                return data[0];
            })
        ;
        return this;
    }

    public async execute() {
        let self = this;
        this
            .pushStage(async (pipelines) => {
                this.aggregation.append(pipelines);
                return await this.aggregation.exec();
            })
        ;

        let request: Promise<any> = Promise.resolve({});

        let stages = _.concat(this.stages, this.postStages);

        for (let stage of stages) {
            request = request.then(async (data) => {
                let result = stage(data);
                if (result instanceof Promise) {
                    return await result;
                } else {
                    return result;
                }
            });
        }

        request = request.catch((e) => {
            Logger.error(e.stack);
        });

        return request;
    }


}
