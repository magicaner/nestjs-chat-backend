export type RequestData = {
    context?: string|string[],
    filter?: {[key:string]: string}
}
export enum SortOrder {
    asce = "asce",
    asc = "asc",
    desc = "desc"
}

export type Pagination = {
    size: number
    num: number
}
export type Sort = {
    field: string,
    direction: SortOrder
}
export type Filter = {
    query?: string,
    name?: string|string[],
    [key: string]: any
}

export type FilterMapValue = {
    ignore?: boolean,
    checkIfExists?: boolean,
    name?: string,
    function?: (field: string, value: any) => object
}

export type FilterMap = {
    [key: string]: FilterMapValue | string | ((field: string, value: any) => object)
}
