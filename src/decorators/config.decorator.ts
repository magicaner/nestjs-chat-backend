import {ConfigService} from "@nestjs/config";
import { app } from "../utils/application.utils";

export const Config = (path: string): any => {
    return (target: any, propertyKey: string, descriptor: PropertyDescriptor) => {
        return app().get<ConfigService>(ConfigService).get(path);
    };
}
