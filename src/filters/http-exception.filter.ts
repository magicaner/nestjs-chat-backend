import { ExceptionFilter, Catch, ArgumentsHost, HttpException } from '@nestjs/common';
import { Request, Response } from 'express';
import {ToObjectInterface} from "../interfaces/to-object.interface";

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
    catch(exception: HttpException & ToObjectInterface, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const request = ctx.getRequest<Request>();
        const status = exception.getStatus();
        const message = exception.getResponse();

        let responseData = {};
        if ("toObject" in exception) {
            responseData = Object.assign(
                exception.toObject(),
            );

        } else {
            responseData = {
                statusCode: status,
                message: message,
            };
        }

        response
            .status(status)
            .json(responseData);
    }
}
