import {Controller, Get, Inject, Post, Req} from '@nestjs/common';
import {ModuleRef} from '@nestjs/core';
import {Request} from "express";
import {ConfigProvider} from "../providers/config.provider";
import _ from "lodash";
import {Config} from "../model/config/config.schema";
import {Model} from "mongoose";
import {InjectModel} from "@nestjs/mongoose";

@Controller('config')
export class ConfigController {
    constructor(
        @InjectModel(Config.name)
        public configModel: Model<Config>,

        @Inject(ConfigProvider.name)
        private configProvider: ConfigProvider,

        private moduleRef: ModuleRef
    ) {
    }
    @Post('/get')
    async get(@Req() req: Request) {
        return this.configProvider.getData();
    }

    @Post('/set')
    async set(@Req() req: Request) {
        if (!_.isEmpty(req.body)) {
            let body = <object>req.body;
            for (let key in body) {
                this.configProvider.set(key, body[key]);
            }
            await this.configProvider.save();
        }
        await this.configProvider.refresh();
        let configData = this.configProvider.getData();
        return configData;
    }
}
