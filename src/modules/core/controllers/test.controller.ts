import { Controller, All, Req } from '@nestjs/common';
import {isEmpty} from "lodash";
import { Request } from 'express';
@Controller()
export class TestController {
    @All('/test')
    async test(@Req() req: Request) {
        return !isEmpty(req.query) ? req.query : req.body;
    }
}
