import _ from "lodash";
import {ConfigProviderInterface} from "../../../interfaces/config-provider.interface";
import {Injectable} from "@nestjs/common";
import {ConfigRepository} from "../model/config/config.repository";

@Injectable()
export class ConfigProvider implements ConfigProviderInterface
{
    protected data: { [key: string]: any} = {};
    protected initialized: boolean = false;
    constructor(
        private configRepository: ConfigRepository,
    ) {
    }

    public async init(force = false)
    {
        if (this.initialized && !force) {
            return;
        }
        this.data = {};
        let configCollection = await this.configRepository.getCollection();

        for (let configModel of await configCollection.getItems()) {
            this.data[configModel.name] = configModel.value;
        }
        this.initialized = true;
    }

    public getData(): {[key:string]: any}
    {
        return _.mapValues(this.data, this.parseValue);
    }
    public setData(data: {[key:string]: any}): this
    {
        data = _.mapValues(this.data, this.stringifyValue);
        this.data = data;
        return this;
    }
    public addData(data: {[key:string]: any}): this
    {
        for (let key in data) {
            this.set(key, data[key]);
        }
        return this;
    }
    public has(path: string, checkIfEmpty: boolean = false): boolean {
        let result = !_.isUndefined(this.data[path]);
        result = checkIfEmpty ? !_.isEmpty(this.get(path)) : result;
        return result;
    }

    public get(path: string, defaultValue: any = null) {
        let value = this.data[path] ?? defaultValue;
        return this.parseValue(value);
    }

    public set(path: string, value: any): this {
        this.data[path] = this.stringifyValue(value);
        return this;
    }

    public unset(path: string): this {
        delete this.data[path];
        return this;
    }

    public async refresh(): Promise<this> {
        await this.init(true)
        return this;
    }

    public async save(): Promise<this>
    {
        let configCollection = await this.configRepository.getCollection();

        for (let key in this.data) {
            let configModel = await configCollection.getItemByFieldName(key, 'name');
            if (!configModel) {
                configModel = configCollection.createNewItem();
            }

            configModel.set('name', key);
            configModel.set('value', this.data[key]);
            if (configModel.isModified()) {
                await this.configRepository.save(configModel);
            }
        }
        return this;
    }

    private stringifyValue(value: any): string
    {
        if (!_.isString(value)) {
            value = JSON.stringify(value);
        }
        return value;
    }

    private parseValue(value: string): any
    {
        try {
            value = JSON.parse(value);
        } catch (e) {}
        return value;
    }
}
