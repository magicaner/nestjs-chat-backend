import {Module} from '@nestjs/common';
import {ConfigModule} from "@nestjs/config";
import {TestController} from './controllers/test.controller';
import {ConfigController} from './controllers/config.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {Config, ConfigSchema} from "./model/config/config.schema";
import {ConfigRepository} from "./model/config/config.repository";
import {ConfigProvider} from "./providers/config.provider";

@Module({
    imports: [
        ConfigModule,
        MongooseModule.forFeature([
            { name: Config.name, schema: ConfigSchema },
        ])
    ],
    controllers: [
        TestController,
        ConfigController,
    ],
    providers: [
        ConfigRepository,
        {
            provide: ConfigProvider.name,
            useFactory: async (configRepository: ConfigRepository) => {
                const configProvider = new ConfigProvider(configRepository);
                await configProvider.init();
                return configProvider;
            },
            inject: [ConfigRepository],
        },
    ],
})
export class CoreModule {}
