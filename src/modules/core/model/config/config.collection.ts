import {Injectable, Scope} from "@nestjs/common";
import {CollectionAbstract, DEFAULT_SORT_DIRECTION, SortType} from "../../../../lib/mongoose/collection-abstract";
import {Config, ConfigDocument} from "./config.schema";
import {Model} from "mongoose";
import {InjectModel} from "@nestjs/mongoose";

@Injectable({ scope: Scope.TRANSIENT})
export class ConfigCollection extends CollectionAbstract<Config,ConfigDocument>
{
    constructor(
        @InjectModel(Config.name) model?: Model<Config>) {
        super(model);
    }
    getDefaultOrder(): SortType | null {
        return {
            field: 'name',
            direction: DEFAULT_SORT_DIRECTION
        };
    }
}
