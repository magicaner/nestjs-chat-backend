import {Model} from "mongoose";
import {Config, ConfigDocument} from "./config.schema";
import {InjectModel} from "@nestjs/mongoose";
import {Injectable, Scope, Inject} from "@nestjs/common";
import {ModuleRef} from "@nestjs/core";
import {ConfigCollection} from "./config.collection";

@Injectable({ scope: Scope.TRANSIENT})
export class ConfigRepository {

    constructor(
        @InjectModel(Config.name)
        public configModel: Model<Config>,

        private moduleRef: ModuleRef
    ) {
        let t = 1;
    }
    create(name:string, value: String|null = null): ConfigDocument
    {
        return new this.configModel({
            "name": name,
            "value": value
        });
    }

    async save(model: ConfigDocument)
    {
        return model.save();
    }

    async getByName(name: string)
    {
        let config = await this.configModel.findOne({name: name}).exec();
        if (!config) {
            config = this.create(name);
        }

        return config;
    }

    async getCollection(filter: Object = {}): Promise<ConfigCollection>
    {
        return await this.moduleRef.create(ConfigCollection);
    }
}
