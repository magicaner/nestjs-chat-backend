import {Module} from '@nestjs/common';
import {AuthService} from './auth.service';
import {UsersModule} from '../users/users.module';
import {JwtModule} from '@nestjs/jwt';
import {AuthController} from './auth.controller';
import {ConfigModule, ConfigService} from "@nestjs/config";

@Module({
    imports: [
        UsersModule,
        JwtModule.registerAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory: async (configService: ConfigService) => ({
                global: true,
                secret: configService.get('APP_SECRET'),
                signOptions: {expiresIn: '31556926s'}, // 1 year
            })
        }),
    ],
    providers: [AuthService, ConfigService],
    controllers: [AuthController],
    exports: [AuthService],
})
export class AuthModule {
}
