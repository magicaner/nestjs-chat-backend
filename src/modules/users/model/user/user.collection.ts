import {Injectable, Scope} from "@nestjs/common";
import {CollectionAbstract, DEFAULT_SORT_DIRECTION, SortType} from "../../../../lib/mongoose/collection-abstract";
import {User, UserDocument} from "./user.schema";
import {Model} from "mongoose";
import {InjectModel} from "@nestjs/mongoose";

@Injectable({ scope: Scope.TRANSIENT})
export class UserCollection extends CollectionAbstract<User,UserDocument>
{
    constructor(
        @InjectModel(User.name) model?: Model<User>) {
        super(model);
    }
    getDefaultOrder(): SortType | null {
        return {
            field: 'name',
            direction: DEFAULT_SORT_DIRECTION
        };
    }
}
