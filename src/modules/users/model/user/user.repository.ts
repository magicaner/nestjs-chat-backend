import {Model} from "mongoose";
import {User, UserDocument} from "./user.schema";
import {InjectModel} from "@nestjs/mongoose";
import {Injectable, Scope, Inject} from "@nestjs/common";
import {ModuleRef} from "@nestjs/core";
import {UserCollection} from "./user.collection";

@Injectable({ scope: Scope.TRANSIENT})
export class UserRepository {

    constructor(
        @InjectModel(User.name)
        public configModel: Model<User>,

        private moduleRef: ModuleRef
    ) {
        let t = 1;
    }
    create(name:string, value: String|null = null): UserDocument
    {
        return new this.configModel({
            "name": name,
            "value": value
        });
    }

    async save(model: UserDocument)
    {
        return model.save();
    }

    async getByName(name: string)
    {
        let config = await this.configModel.findOne({name: name}).exec();
        if (!config) {
            config = this.create(name);
        }

        return config;
    }

    async getCollection(filter: Object = {}): Promise<UserCollection>
    {
        return await this.moduleRef.create(UserCollection);
    }
}
