import {Injectable, Logger} from '@nestjs/common';
import { Socket } from 'socket.io';

@Injectable()
export class ChatService {
    private readonly connectedClients: Map<string, Socket> = new Map();

    constructor(private readonly logger: Logger) {
        let t = 1;
    }

    handleConnection(socket: Socket): void {
        this.logger.log('test');
        const clientId = socket.id;
        this.connectedClients.set(clientId, socket);

        socket.on('disconnect', () => {
            this.connectedClients.delete(clientId);
        });

        // Handle other events and messages from the client
    }

    // Add more methods for handling events, messages, etc.
}
