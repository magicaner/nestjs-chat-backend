import {
    ConnectedSocket,
    MessageBody,
    OnGatewayConnection, OnGatewayDisconnect,
    SubscribeMessage,
    WebSocketGateway,
    WebSocketServer
} from "@nestjs/websockets";
import {Server, Socket} from 'socket.io';
import io from 'socket.io';
import {ChatService} from "./chat.service";
import {Logger, UseGuards} from "@nestjs/common";
import {AuthService, JwtAccessToken} from "../auth/auth.service";
import {ChatGuard} from "./chat.guard";

type Message = {
    "toSocketId": string
    "from": string,
    "to": string,
    "content": string,
    "timestamp": string
}

@WebSocketGateway({
    cors: {
        "origin": "*",
        "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
        "preflightContinue": false,
        "optionsSuccessStatus": 204
    }
})
export class ChatGateway implements OnGatewayConnection, OnGatewayDisconnect {

    @WebSocketServer()
    private server: Server;

    constructor(
        private readonly chatService: ChatService,
        private readonly logger: Logger,
        private readonly authService: AuthService
    ) {
    }

    @UseGuards(ChatGuard)
    @SubscribeMessage('init')
    async handleLogin(
        @MessageBody('accessToken') accessToken: string,
        @ConnectedSocket() client: Socket,
    ) {
        let users = [];
        for (let [id, socket] of this.server.of("/").sockets) {
            users.push({
                socketId: id,
                id: socket['userId'],
                username: socket['username'],
                isOnline: 1,
            });
        }

        this.server.emit('lobby', users);
        return;
    }

    @UseGuards(ChatGuard)
    @SubscribeMessage('message')
    handleMessage(@MessageBody() data: any): string {
        this.server.to(data.toSocketId).emit('message', data); // Broadcast the message to all connected clients
        this.logger.log(data);
        return data;
    }

    handleConnection(socket: Socket): void {
        this.chatService.handleConnection(socket);
        this.logger.log('connection happened')
        this.server.emit("connection", "test")
    }



    handleDisconnect(client: any) {
        // Handle disconnection event
        this.logger.log('disconnection happened')
    }
}
