import {Logger, Module} from '@nestjs/common';
import {ChatGateway} from "./chat.gateway";
import {ChatService} from "./chat.service";
import {AuthService} from "../auth/auth.service";
import {AuthModule} from "../auth/auth.module";
import {ChatGuard} from "./chat.guard";

@Module({
    imports:[
        AuthModule
    ],
    providers: [
        Logger,
        ChatGateway,
        ChatService
    ],
})
export class ChatModule {
}
