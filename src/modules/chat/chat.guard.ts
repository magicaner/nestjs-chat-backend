import {
    CanActivate,
    ExecutionContext,
    Injectable, Logger,
    UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import {Socket} from "socket.io";
import {AuthService} from "../auth/auth.service";

@Injectable()
export class ChatGuard implements CanActivate {
    constructor(
        private authService: AuthService,
        private logger: Logger
    ) {}

    async canActivate(context: ExecutionContext): Promise<boolean> {
        const socket = context.switchToWs().getClient();
        const token = this.extractTokenFromHeader(socket);
        if (!token) {
            throw new UnauthorizedException();
        }
        try {
            const payload = await this.authService.verify(token);
            // 💡 We're assigning the payload to the request object here
            // so that we can access it in our route handlers
            //request['user'] = payload;
            socket.userId = payload.sub;
            socket.username = payload.username;
        } catch {
            throw new UnauthorizedException();
        }
        return true;
    }

    private extractTokenFromHeader(socket: Socket): string | undefined {
        if (!socket.handshake.auth.token) {
            return undefined;
        }
        const [type, token] = socket.handshake.auth.token.split(' ') ?? [];
        return type === 'Bearer' ? token : undefined;
    }
}
