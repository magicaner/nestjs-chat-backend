import {Prop, Schema, SchemaFactory} from '@nestjs/mongoose';
import {HydratedDocument} from 'mongoose';

export type MessageDocument = HydratedDocument<Message>;

@Schema({
    versionKey: false,
})
export class Message {
    @Prop() username: string;
    @Prop() password: string;
}

export const MessageSchema = SchemaFactory.createForClass(Message);
