import {Model} from "mongoose";
import {Message, MessageDocument} from "./message.schema";
import {InjectModel} from "@nestjs/mongoose";
import {Injectable, Scope} from "@nestjs/common";
import {ModuleRef} from "@nestjs/core";
import {UserCollection} from "./message.collection";

@Injectable({scope: Scope.TRANSIENT})
export class MessageRepository {

    constructor(
        @InjectModel(Message.name)
        public configModel: Model<Message>,
        private moduleRef: ModuleRef
    ) {
        let t = 1;
    }

    create(name: string, value: String | null = null): MessageDocument {
        return new this.configModel({
            "name": name,
            "value": value
        });
    }

    async save(model: MessageDocument) {
        return model.save();
    }

    async getByName(name: string) {
        let config = await this.configModel.findOne({name: name}).exec();
        if (!config) {
            config = this.create(name);
        }

        return config;
    }

    async getCollection(filter: Object = {}): Promise<UserCollection> {
        return await this.moduleRef.create(UserCollection);
    }
}
