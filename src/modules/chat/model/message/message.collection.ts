import {Injectable, Scope} from "@nestjs/common";
import {CollectionAbstract, DEFAULT_SORT_DIRECTION, SortType} from "../../../../lib/mongoose/collection-abstract";
import {Message, MessageDocument} from "./message.schema";
import {Model} from "mongoose";
import {InjectModel} from "@nestjs/mongoose";

@Injectable({ scope: Scope.TRANSIENT})
export class UserCollection extends CollectionAbstract<Message,MessageDocument>
{
    constructor(
        @InjectModel(Message.name) model?: Model<Message>) {
        super(model);
    }
    getDefaultOrder(): SortType | null {
        return {
            field: 'name',
            direction: DEFAULT_SORT_DIRECTION
        };
    }
}
