import {NestFactory} from '@nestjs/core';
import {ConfigService} from '@nestjs/config';
import {Logger} from '@nestjs/common';
import {
    FastifyAdapter,
    NestFastifyApplication,
} from '@nestjs/platform-fastify';

import {AppModule} from './app.module';
import {HttpExceptionFilter} from "./filters/http-exception.filter";
import {application} from "./utils/application.utils";

export default async function bootstrap() {
    const app = await NestFactory.create<NestFastifyApplication>(
        AppModule,
        new FastifyAdapter({
            bodyLimit: 50 * 1024 * 1024, // 50MB
            logger: true
        })
    );

    application.init(app);


    app.useGlobalFilters(new HttpExceptionFilter());
    const configService: ConfigService = app.get<ConfigService>(ConfigService);
    const port = configService.get('PORT');
    app.setGlobalPrefix(configService.get('BASE_PREFIX'));

    //app.use(express.json({limit: '50mb'}));
    //app.use(express.urlencoded({limit: '50mb'}));
    await app.listen(port, '0.0.0.0');
    Logger.log(`Application is running on: ${await app.getUrl()}`, 'Application');
}
