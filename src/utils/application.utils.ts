import { INestApplication, INestApplicationContext, INestMicroservice } from '@nestjs/common';
class Application {
    private app: INestApplication;
    init(app: INestApplication): Application {
        this.app = app;
        return this;
    }

    getInstance(): INestApplication {
        return this.app;
    }
}

export const application = new Application();
export const app = (): INestApplication => {
    return application.getInstance();
}
