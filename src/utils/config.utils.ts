import {ConfigService} from "@nestjs/config";
import {app} from "./application.utils";

export const config = (path) => {
    return app().get<ConfigService>(ConfigService).get(path);
}
