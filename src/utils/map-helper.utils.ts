
class MapHelperUtils
{
    public map(data, map): any
    {
        let mappedData = {};
        for (let requestField in map) {
            if (requestField in data) {
                mappedData[map[requestField]] = data[requestField];
            } else {
                mappedData[requestField] = data[requestField];
            }
        }
        return mappedData;
    }
}

export const mapHelper = new MapHelperUtils();
