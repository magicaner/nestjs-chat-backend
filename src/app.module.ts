import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {ConfigModule, ConfigService} from '@nestjs/config';
import {MongooseModule} from '@nestjs/mongoose';
import { CoreModule } from './modules/core/core.module';
import {FactoryService} from "./providers/factory.service";
import { AuthModule } from './modules/auth/auth.module';
import { UsersModule } from './modules/users/users.module';
import { ChatModule } from './modules/chat/chat.module';

@Module({
    imports: [
        ConfigModule.forRoot({
            envFilePath: ['.env.defaults', '.env'],
        }),
        MongooseModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
                uri: configService.get('MONGO_URL'),
            }),
            inject: [ConfigService],
        }),
        CoreModule,
        AuthModule,
        UsersModule,
        ChatModule
    ],
    controllers: [AppController],
    providers: [
        AppService,
        FactoryService
    ]
})
export class AppModule {
}
